#Roga and Copyta (Test Task)
 
**Tools:**  
JDK 8, Spring 4 (MVC, Data, Boot), Twitter Bootstrap 3, JUnit 4, Mockito, Maven 3, Git / Bitbucket, PostgreSQL, IntelliJIDEA 16.  
 
 
**Notes:**  
SQL ddl is located in the `src\main\resources\ddl\ddl.sql`
 
--  
**Dmitryy Bezfamilnyy**