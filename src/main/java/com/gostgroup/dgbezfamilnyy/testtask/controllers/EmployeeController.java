package com.gostgroup.dgbezfamilnyy.testtask.controllers;

import com.gostgroup.dgbezfamilnyy.testtask.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void fireEmployee(@PathVariable("id") int idOfEmployee) {
        employeeService.fireEmployee(idOfEmployee);
    }
}
