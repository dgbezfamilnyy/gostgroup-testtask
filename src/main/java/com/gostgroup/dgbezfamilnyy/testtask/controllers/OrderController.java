package com.gostgroup.dgbezfamilnyy.testtask.controllers;

import com.gostgroup.dgbezfamilnyy.testtask.model.Order;
import com.gostgroup.dgbezfamilnyy.testtask.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/order/")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @RequestMapping(method = RequestMethod.POST)
    public Order addOrder(@RequestBody Order order) {
        return orderService.add(order);
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<Order> getAllOrders() {
        return orderService.getAll();
    }

    @RequestMapping(value = "allNotCompleted", method = RequestMethod.GET)
    public List<Order> getAllNotCompletedOrders() {
        return orderService.getAllNotCompletedOrders();
    }

    @RequestMapping(value = "all/{idOfDepartment}", method = RequestMethod.GET)
    public List<Order> getAllOrdersInDepartment(@PathVariable("idOfDepartment") int idOfDepartment) {
        return orderService.getAllByDepartment(idOfDepartment);
    }

    @RequestMapping(value = "all/{idOfEmployee}", method = RequestMethod.GET)
    public List<Order> getAllOrdersByEmployee(@PathVariable("idOfEmployee") int idOfEmployee) {
        return orderService.getAllByEmployee(idOfEmployee);
    }

}
