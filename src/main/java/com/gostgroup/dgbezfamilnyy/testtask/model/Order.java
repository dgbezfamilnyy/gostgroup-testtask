package com.gostgroup.dgbezfamilnyy.testtask.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "\"order\"")
public class Order {
    @Id
    private int id;

    @Column(name = "date_of_end", columnDefinition = "TIMESTAMP")
    private LocalDateTime dateOfEnd;

    @ManyToOne
    @JoinColumn(name = "furniture")
    private Furniture furniture;

    @ManyToOne
    @JoinColumn(name = "employee")
    private Employee employee;

    @ManyToOne
    @JoinColumn(name = "department")
    private Department department;

    @ManyToOne
    @JoinColumn(name = "status_of_order")
    private StatusOfOrder statusOfOrder;

    public Order() {
    }

    public Order(int id, LocalDateTime dateOfEnd, Furniture furniture, Employee employee, Department department, StatusOfOrder statusOfOrder) {
        this.id = id;
        this.dateOfEnd = dateOfEnd;
        this.furniture = furniture;
        this.employee = employee;
        this.department = department;
        this.statusOfOrder = statusOfOrder;
    }

    public Furniture getFurniture() {
        return furniture;
    }

    public void setFurniture(Furniture furniture) {
        this.furniture = furniture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDateOfEnd() {
        return dateOfEnd;
    }

    public void setDateOfEnd(LocalDateTime dateOfEnd) {
        this.dateOfEnd = dateOfEnd;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public StatusOfOrder getStatusOfOrder() {
        return statusOfOrder;
    }

    public void setStatusOfOrder(StatusOfOrder statusOfOrder) {
        this.statusOfOrder = statusOfOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;

        Order order = (Order) o;

        if (id != order.id) return false;
        return dateOfEnd != null ? dateOfEnd.equals(order.dateOfEnd) : order.dateOfEnd == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (dateOfEnd != null ? dateOfEnd.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", dateOfEnd=" + dateOfEnd +
                ", furniture=" + furniture +
                ", employee=" + employee +
                ", department=" + department +
                ", statusOfOrder=" + statusOfOrder +
                '}';
    }
}
