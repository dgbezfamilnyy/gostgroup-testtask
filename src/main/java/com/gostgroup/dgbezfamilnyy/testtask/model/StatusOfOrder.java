package com.gostgroup.dgbezfamilnyy.testtask.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "\"status_of_order\"")
public class StatusOfOrder {
    @Id
    private int id;

    private String status;

    @OneToMany(mappedBy = "statusOfOrder")
    private Set<Order> orders;

    public StatusOfOrder() {
    }

    public StatusOfOrder(int id, String status, Set<Order> orders) {
        this.id = id;
        this.status = status;
        this.orders = orders;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StatusOfOrder)) return false;

        StatusOfOrder that = (StatusOfOrder) o;

        return status != null ? status.equals(that.status) : that.status == null;
    }

    @Override
    public int hashCode() {
        return status != null ? status.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "StatusOfOrder{" +
                "id=" + id +
                ", status='" + status + '\'' +
                '}';
    }
}
