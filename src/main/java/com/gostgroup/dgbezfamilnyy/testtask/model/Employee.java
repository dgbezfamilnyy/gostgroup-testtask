package com.gostgroup.dgbezfamilnyy.testtask.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "\"employee\"")
public class Employee {
    @Id
    private int id;

    private String email;

    private String password;

    private String name;

    private String surname;

    private String middlename;

    @OneToMany(mappedBy = "employee")
    private Set<Order> orders;

    @ManyToOne
    @JoinColumn(name = "department")
    private Department department;

    @ManyToOne
    @JoinColumn(name = "status_of_employee")
    private StatusOfEmployee statusOfEmployee;

    public Employee() {
    }

    public Employee(int id, String email, String password, String name, String surname, String middlename, Set<Order> orders, Department department, StatusOfEmployee statusOfEmployee) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.middlename = middlename;
        this.orders = orders;
        this.department = department;
        this.statusOfEmployee = statusOfEmployee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public StatusOfEmployee getStatusOfEmployee() {
        return statusOfEmployee;
    }

    public void setStatusOfEmployee(StatusOfEmployee statusOfEmployee) {
        this.statusOfEmployee = statusOfEmployee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;

        Employee employee = (Employee) o;

        if (!email.equals(employee.email)) return false;
        if (!password.equals(employee.password)) return false;
        if (!name.equals(employee.name)) return false;
        if (surname != null ? !surname.equals(employee.surname) : employee.surname != null) return false;
        return middlename != null ? middlename.equals(employee.middlename) : employee.middlename == null;
    }

    @Override
    public int hashCode() {
        int result = email.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (middlename != null ? middlename.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middlename='" + middlename + '\'' +
                ", department=" + department +
                ", statusOfEmployee=" + statusOfEmployee +
                '}';
    }
}
