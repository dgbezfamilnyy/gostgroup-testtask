package com.gostgroup.dgbezfamilnyy.testtask.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "\"furniture\"")
public class Furniture {
    @Id
    private int id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "department")
    private Department department;

    @OneToMany(mappedBy = "furniture")
    private Set<Order> orders;

    public Furniture() {
    }

    public Furniture(int id, String name, Department department, Set<Order> orders) {
        this.id = id;
        this.name = name;
        this.department = department;
        this.orders = orders;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Furniture)) return false;

        Furniture furniture = (Furniture) o;

        return name.equals(furniture.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "Furniture{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", department=" + department +
                '}';
    }
}
