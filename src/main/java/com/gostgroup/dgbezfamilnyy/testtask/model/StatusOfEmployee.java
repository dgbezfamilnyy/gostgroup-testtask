package com.gostgroup.dgbezfamilnyy.testtask.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "\"status_of_employee\"")
public class StatusOfEmployee {
    @Id
    private int id;

    private String status;

    @OneToMany(mappedBy = "statusOfEmployee")
    private Set<Employee> employees;

    public StatusOfEmployee() {
    }

    public StatusOfEmployee(int id, String status, Set<Employee> employees) {
        this.id = id;
        this.status = status;
        this.employees = employees;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StatusOfEmployee)) return false;

        StatusOfEmployee that = (StatusOfEmployee) o;

        return status.equals(that.status);
    }

    @Override
    public int hashCode() {
        return status.hashCode();
    }

    @Override
    public String toString() {
        return "StatusOfEmployee{" +
                "id=" + id +
                ", status='" + status + '\'' +
                '}';
    }
}
