package com.gostgroup.dgbezfamilnyy.testtask.service;

import org.springframework.transaction.annotation.Transactional;

public interface EmployeeService {
    @Transactional
    void fireEmployee(int idOfEmployee);
}
