package com.gostgroup.dgbezfamilnyy.testtask.service;

import com.gostgroup.dgbezfamilnyy.testtask.model.Order;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface OrderService {

    List<Order> getAll();

    @Transactional
    Order add(Order order);

    @Transactional
    List<Order> getAllNotCompletedOrders();

    @Transactional
    List<Order> getAllByDepartment(int idOfDepartment);

    @Transactional
    List<Order> getAllByEmployee(int idOfEmployee);

}
