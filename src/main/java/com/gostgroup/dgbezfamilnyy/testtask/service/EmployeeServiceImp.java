package com.gostgroup.dgbezfamilnyy.testtask.service;

import com.gostgroup.dgbezfamilnyy.testtask.dao.EmployeeRepository;
import com.gostgroup.dgbezfamilnyy.testtask.dao.OrderRepository;
import com.gostgroup.dgbezfamilnyy.testtask.dao.StatusOfEmployeeRepository;
import com.gostgroup.dgbezfamilnyy.testtask.dao.StatusOfOrderRepository;
import com.gostgroup.dgbezfamilnyy.testtask.model.Employee;
import com.gostgroup.dgbezfamilnyy.testtask.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImp implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private StatusOfEmployeeRepository statusOfEmployeeRepository;
    @Autowired
    private StatusOfOrderRepository statusOfOrderRepository;
    @Autowired
    private OrderRepository orderRepository;

    @Override
    public void fireEmployee(int idOfEmployee) {
        //firing employee
        Employee employee = employeeRepository.findOne(idOfEmployee);
        employee.setStatusOfEmployee(statusOfEmployeeRepository.findByStatus(StatusOfEmployeeService.FIRED));

        //check current order of this employee
        Order currentOrder = getCurrentOrderOfEmployee(idOfEmployee);
        if (currentOrder != null) {
            // if order exist, then search free employee for this order
            List<Employee> freeEmployees = employeeRepository.findByDepartmentIdAndStatusOfEmployeeStatus(employee.getDepartment().getId(), StatusOfEmployeeService.AWAIT_ORDER);
            if (freeEmployees != null && freeEmployees.size() > 0) {
                currentOrder.setEmployee(freeEmployees.get(0));
            } else {
                // if free employee not found
                currentOrder.setStatusOfOrder(statusOfOrderRepository.findByStatus(StatusOfOrderService.APPOINTED_TO_DEPARTMENT));
            }
        }
    }

    private Order getCurrentOrderOfEmployee(int idOfEmployee) {
        List<Order> orders = orderRepository.findByEmployeeIdAndStatusOfOrderStatus(idOfEmployee, StatusOfOrderService.APPOINTED_TO_EMPLOYEE);
        if (orders != null && orders.size() > 0) {
            return orders.get(0);
        } else {
            return null;
        }
    }
}
