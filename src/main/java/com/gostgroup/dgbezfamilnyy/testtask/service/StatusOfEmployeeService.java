package com.gostgroup.dgbezfamilnyy.testtask.service;

public interface StatusOfEmployeeService {
    String AWAIT_ORDER = "AWAIT_ORDER";
    String FIRED = "FIRED";
}
