package com.gostgroup.dgbezfamilnyy.testtask.service;

public interface StatusOfOrderService {
    String APPOINTED_TO_DEPARTMENT = "APPOINTED_TO_DEPARTMENT";
    String APPOINTED_TO_EMPLOYEE = "APPOINTED_TO_EMPLOYEE";
    String ERROR_DURING_APPOINTING = "ERROR_DURING_APPOINTING";
    String COMPLETED = "COMPLETED";
    String DEPARTMENT = "department";
    String EMPLOYEE = "employee";
}
