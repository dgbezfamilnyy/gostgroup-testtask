package com.gostgroup.dgbezfamilnyy.testtask.service;

import com.gostgroup.dgbezfamilnyy.testtask.dao.EmployeeRepository;
import com.gostgroup.dgbezfamilnyy.testtask.dao.FurnitureRepository;
import com.gostgroup.dgbezfamilnyy.testtask.dao.OrderRepository;
import com.gostgroup.dgbezfamilnyy.testtask.dao.StatusOfOrderRepository;
import com.gostgroup.dgbezfamilnyy.testtask.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImp implements OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private FurnitureRepository furnitureRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private StatusOfOrderRepository statusOfOrderRepository;

    @Override
    public Order add(Order order) {

        Furniture furniture = furnitureRepository.findOne(order.getFurniture().getId());
        if (furniture != null) {
            //set department
            Department department = furniture.getDepartment();
            order.setDepartment(department);
            //set employee
            List<Employee> freeEmployees = employeeRepository.findByDepartmentIdAndStatusOfEmployeeStatus(department.getId(), StatusOfEmployeeService.AWAIT_ORDER);
            if (freeEmployees != null && freeEmployees.size() > 0) {
                order.setEmployee(freeEmployees.get(0));
            }
            //set status
            if (order.getEmployee() != null) {
                order.setStatusOfOrder(appointedTo(StatusOfOrderService.EMPLOYEE));
            } else {
                order.setStatusOfOrder(appointedTo(StatusOfOrderService.DEPARTMENT));
            }
        }

        return orderRepository.save(order);
    }

    @Override
    public List<Order> getAll() {
        List<Order> orders = new ArrayList<>();
        orderRepository.findAll().forEach(orders::add);
        return orders;
    }

    @Override
    public List<Order> getAllNotCompletedOrders() {
        List<Order> orders = orderRepository.findByStatusOfOrderStatusNot(StatusOfOrderService.COMPLETED);
        // select addition info from db
        selectAddInfoFromDB(orders);
        return orders;
    }

    @Override
    public List<Order> getAllByDepartment(int idOfDepartment) {
        List<Order> orders = orderRepository.findByDepartmentId(idOfDepartment);
        // select addition info from db
        selectAddInfoFromDB(orders);
        return orders;
    }

    @Override
    public List<Order> getAllByEmployee(int idOfEmployee) {
        List<Order> orders = orderRepository.findByEmployeeId(idOfEmployee);
        // select addition info from db
        selectAddInfoFromDB(orders);
        return orders;
    }

    private void selectAddInfoFromDB(List<Order> orders) {
        if (orders != null) {
            for (Order order : orders) {
                order.getDepartment();
                order.getEmployee();
                order.getFurniture();
                order.getStatusOfOrder();
            }
        }
    }

    private StatusOfOrder appointedTo(String who) {
        switch (who) {
            case StatusOfOrderService.DEPARTMENT:
                return statusOfOrderRepository.findByStatus(StatusOfOrderService.APPOINTED_TO_DEPARTMENT);

            case StatusOfOrderService.EMPLOYEE:
                return statusOfOrderRepository.findByStatus(StatusOfOrderService.APPOINTED_TO_EMPLOYEE);

            default:
                return statusOfOrderRepository.findByStatus(StatusOfOrderService.ERROR_DURING_APPOINTING);
        }
    }
}
