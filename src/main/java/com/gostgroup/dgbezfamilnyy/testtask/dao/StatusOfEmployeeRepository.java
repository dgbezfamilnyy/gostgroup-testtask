package com.gostgroup.dgbezfamilnyy.testtask.dao;

import com.gostgroup.dgbezfamilnyy.testtask.model.StatusOfEmployee;
import org.springframework.data.repository.CrudRepository;

public interface StatusOfEmployeeRepository extends CrudRepository<StatusOfEmployee, Integer> {
    StatusOfEmployee findByStatus(String status);
}
