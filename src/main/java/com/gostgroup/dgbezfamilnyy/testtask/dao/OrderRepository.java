package com.gostgroup.dgbezfamilnyy.testtask.dao;

import com.gostgroup.dgbezfamilnyy.testtask.model.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Integer> {
    List<Order> findByStatusOfOrderStatusNot(String status);

    List<Order> findByDepartmentId(int idOfDepartment);

    List<Order> findByEmployeeId(int idOfEmployee);

    List<Order> findByEmployeeIdAndStatusOfOrderStatus(int idOfEmployee, String status);
}
