package com.gostgroup.dgbezfamilnyy.testtask.dao;

import com.gostgroup.dgbezfamilnyy.testtask.model.StatusOfOrder;
import org.springframework.data.repository.CrudRepository;

public interface StatusOfOrderRepository extends CrudRepository<StatusOfOrder, Integer> {

    StatusOfOrder findByStatus(String status);
}
