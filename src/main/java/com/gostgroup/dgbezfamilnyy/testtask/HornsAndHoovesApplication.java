package com.gostgroup.dgbezfamilnyy.testtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HornsAndHoovesApplication {

    public static void main(String[] args) {
        SpringApplication.run(HornsAndHoovesApplication.class, args);
    }
}
