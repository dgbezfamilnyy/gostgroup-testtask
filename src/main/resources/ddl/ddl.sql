CREATE TABLE public.department
(
  id serial NOT NULL,
  status character varying(30)[] NOT NULL,
  PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.department
  OWNER to postgres;

/*next*/

CREATE TABLE public."statusOfEmployee"
(
  id serial NOT NULL,
  status character varying(20)[] NOT NULL,
  PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.status_of_employee
  OWNER to postgres;

/*next*/

CREATE TABLE public."statusOfOrder"
(
  id serial NOT NULL,
  status character varying(20)[] NOT NULL,
  PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.status_of_order
  OWNER to postgres;

/*next*/

CREATE TABLE public.employee
(
  id serial NOT NULL,
  email character varying(20)[] NOT NULL,
  password character varying(150)[] NOT NULL,
  name character varying(30)[] NOT NULL,
  surname character varying(30)[] NOT NULL,
  middlename character varying(30)[],
  "statusOfEmployee" integer,
  department integer,
  PRIMARY KEY (id),
  CONSTRAINT email_constraint UNIQUE (email),
  CONSTRAINT status_fr_key FOREIGN KEY ("statusOfEmployee")
  REFERENCES public.status_of_employee (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE SET NULL,
  CONSTRAINT department_fr_key FOREIGN KEY (department)
  REFERENCES public.department (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
)
WITH (
OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.employee
  OWNER to postgres;

/*next*/

CREATE TABLE public.furniture
(
  id serial NOT NULL,
  name character varying(100)[] NOT NULL,
  department integer,
  PRIMARY KEY (id),
  CONSTRAINT department_fr_key FOREIGN KEY (department)
  REFERENCES public.department (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
)
WITH (
OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.furniture
  OWNER to postgres;

/*next*/

CREATE TABLE public."order"
(
  id serial NOT NULL,
  "dateOfEnd" timestamp without time zone,
  "statusOfOrder" integer,
  furniture integer,
  department integer,
  employee integer,
  PRIMARY KEY (id),
  CONSTRAINT "statusOfOrder_fr_key" FOREIGN KEY ("statusOfOrder")
  REFERENCES public.status_of_order (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT furniture_fr_key FOREIGN KEY (furniture)
  REFERENCES public.furniture (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT department_fr_key FOREIGN KEY (department)
  REFERENCES public.department (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT employee_fr_key FOREIGN KEY (employee)
  REFERENCES public.employee (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
)
WITH (
OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public."order"
  OWNER to postgres;

