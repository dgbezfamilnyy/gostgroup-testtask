package com.gostgroup.dgbezfamilnyy.testtask.dao;

import com.gostgroup.dgbezfamilnyy.testtask.HornsAndHoovesApplication;
import com.gostgroup.dgbezfamilnyy.testtask.model.Employee;
import com.gostgroup.dgbezfamilnyy.testtask.service.StatusOfEmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {HornsAndHoovesApplication.class})
@TestPropertySource(locations = {"classpath:application.properties"})
@Transactional
public class EmployeeRepositoryTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void findByDepartmentIdAndStatusOfEmployeeStatus() {
        List<Employee> employees = employeeRepository.findByDepartmentIdAndStatusOfEmployeeStatus(1, StatusOfEmployeeService.AWAIT_ORDER);
        assertEquals(1, employees.size());
        Employee expected = new Employee(1, "employee@mail.ru", "password", "employeeName",
                "employeeSurname", "employeeMiddleName", null, null, null);
        assertEquals(expected, employees.get(0));
    }
}
