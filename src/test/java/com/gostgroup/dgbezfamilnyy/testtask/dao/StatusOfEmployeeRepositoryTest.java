package com.gostgroup.dgbezfamilnyy.testtask.dao;

import com.gostgroup.dgbezfamilnyy.testtask.HornsAndHoovesApplication;
import com.gostgroup.dgbezfamilnyy.testtask.model.StatusOfEmployee;
import com.gostgroup.dgbezfamilnyy.testtask.service.StatusOfEmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {HornsAndHoovesApplication.class})
@TestPropertySource(locations = {"classpath:application.properties"})
@Transactional
public class StatusOfEmployeeRepositoryTest {
    @Autowired
    private StatusOfEmployeeRepository statusOfEmployeeRepository;

    @Test
    public void findByStatus() {
        StatusOfEmployee statusOfEmployee = statusOfEmployeeRepository.findByStatus(StatusOfEmployeeService.AWAIT_ORDER);
        StatusOfEmployee expected = new StatusOfEmployee(1, StatusOfEmployeeService.AWAIT_ORDER, null);
        assertEquals(expected, statusOfEmployee);
    }
}
