package com.gostgroup.dgbezfamilnyy.testtask.dao;

import com.gostgroup.dgbezfamilnyy.testtask.HornsAndHoovesApplication;
import com.gostgroup.dgbezfamilnyy.testtask.model.Order;
import com.gostgroup.dgbezfamilnyy.testtask.service.StatusOfOrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {HornsAndHoovesApplication.class})
@TestPropertySource(locations = {"classpath:application.properties"})
@Transactional
public class OrderRepositoryTest {
    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void findByStatusOfOrderStatusNot() {
        List<Order> notCompletedOrders = orderRepository.findByStatusOfOrderStatusNot(StatusOfOrderService.COMPLETED);
        List<Order> expected = Arrays.asList(new Order(1, null, null, null, null, null),
                new Order(2, null, null, null, null, null));
        assertEquals(expected,notCompletedOrders);
    }

    @Test
    public void findByDepartmentId() {
        List<Order> orders = orderRepository.findByDepartmentId(1);
        List<Order> expected =  Arrays.asList(new Order(1, null, null, null, null, null),
                new Order(2, null, null, null, null, null));
        assertEquals(expected, orders);
    }

    @Test
    public void findByEmployeeId() {
        List<Order> orders = orderRepository.findByEmployeeId(1);
        List<Order> expected =  Arrays.asList(new Order(2, null, null, null, null, null));
        assertEquals(expected, orders);
    }

    @Test
    public void findByEmployeeIdAndStatusOfOrderStatus() {
        orderRepository.findByEmployeeIdAndStatusOfOrderStatus(1, StatusOfOrderService.APPOINTED_TO_EMPLOYEE);
    }
}
