package com.gostgroup.dgbezfamilnyy.testtask.dao;

import com.gostgroup.dgbezfamilnyy.testtask.HornsAndHoovesApplication;
import com.gostgroup.dgbezfamilnyy.testtask.model.StatusOfOrder;
import com.gostgroup.dgbezfamilnyy.testtask.service.StatusOfOrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {HornsAndHoovesApplication.class})
@TestPropertySource(locations = {"classpath:application.properties"})
@Transactional
public class StatusOfOrderRepositoryTest {
    @Autowired
    private StatusOfOrderRepository statusOfOrderRepository;

    @Test
    public void findByStatus() {
        StatusOfOrder statusOfOrder = statusOfOrderRepository.findByStatus(StatusOfOrderService.APPOINTED_TO_DEPARTMENT);
        StatusOfOrder expected = new StatusOfOrder(1, StatusOfOrderService.APPOINTED_TO_DEPARTMENT, null);
        assertEquals(expected, statusOfOrder);
    }
}
