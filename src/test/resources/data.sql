INSERT INTO "status_of_employee" VALUES
  (1, 'AWAIT_ORDER');

INSERT INTO "department" VALUES
  (1, 'department of upholstered furniture');

INSERT INTO "employee" (id, email, password, name, surname, middlename, department, status_of_employee)
VALUES
  (1, 'employee@mail.ru', 'password', 'employeeName', 'employeeSurname', 'employeeMiddleName', 1, 1);

INSERT INTO "status_of_order" VALUES
  (1, 'APPOINTED_TO_DEPARTMENT'),
  (2, 'APPOINTED_TO_EMPLOYEE');

INSERT INTO "furniture" (id, name, department) VALUES
  (1, 'sofa', 1);

INSERT INTO "order" (id, date_of_end, status_of_order, furniture, department, employee) VALUES
  (1, NULL, 1, 1, 1, NULL),
  (2, NULL, 2, 1, 1, 1);
